package com.example.api.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * The class represents the Balance Sheet model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BalanceSheet {
    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("accountNo")
    private String accountNo;
    @JsonProperty("accountType")
    private String accountType;
    @JsonProperty("balance")
    private BigDecimal balance;
    @JsonProperty("subFundIdBalanceSheet")
    private String subFundIdBalanceSheet;
    @JsonProperty("weightTNav")
    private double weightTNav;

    public String getAccountName() {
        return accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public String getAccountType() {
        return accountType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getSubFundIdBalanceSheet() {
        return subFundIdBalanceSheet;
    }

    public double getWeightTNav() {
        return weightTNav;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setSubFundIdBalanceSheet(String subFundIdBalanceSheet) {
        this.subFundIdBalanceSheet = subFundIdBalanceSheet;
    }

    public void setWeightTNav(double weightTNav) {
        this.weightTNav = weightTNav;
    }

    @Override
    public String toString() {
        return "BalanceSheet[ accountName: " + accountName + ", accountNo: " + accountNo + ", accountType: " + accountType + ", " +
            "balance: " + balance + ", subFundIdBalanceSheet: " + subFundIdBalanceSheet + ", " +
            "weightTNav: " + weightTNav + " ]";
    }
}