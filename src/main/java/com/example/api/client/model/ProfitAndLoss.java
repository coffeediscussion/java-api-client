package com.example.api.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * The class represents the PNL model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfitAndLoss {
    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("accountNo")
    private long accountNo;
    @JsonProperty("accountType")
    private String accountType;
    @JsonProperty("balance")
    private BigDecimal balance;
    @JsonProperty("subFundIdPnl")
    private String subFundIdPnl;
    @JsonProperty("weightTNAV")
    private double weightTNav;

    public String getAccountName() {
        return accountName;
    }

    public long getAccountNo() {
        return accountNo;
    }

    public String getAccountType() {
        return accountType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getSubFundIdPnl() {
        return subFundIdPnl;
    }

    public double getWeightTNav() {
        return weightTNav;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public void setAccountNo(long accountNo) {
        this.accountNo = accountNo;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setSubFundIdPnl(String subFundIdPnl) {
        this.subFundIdPnl = subFundIdPnl;
    }

    public void setWeightTNav(double weightTNav) {
        this.weightTNav = weightTNav;
    }

    public String toString() {
        return "ProfitAndLoss[ accountName: " + accountName + ", " +
            "accountNo: " + accountNo + ", " +
            "accountType: " + accountType + ", " +
            "balance: " + balance + ", subFundIdPnl: " + subFundIdPnl + ", " +
            "weightTNav: " + weightTNav + " ]";
    }
}