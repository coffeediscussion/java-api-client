package com.example.api.client.model;

import java.time.LocalDate;
import java.util.List;

import static java.util.Arrays.*;

/**
 * The class represents a set of attributes to filter the results.
 */
public class Filter {
    /**
     * A nullable list of client codes. The result set will contain data for the intersection of the
     * codes in the list with the codes owned by the user. For example, if the list contains {@code "123"}, {@code "234"} and the user
     * owns {@code "234"}, {@code "456"}, only the data with client code {@code "234"} will be returned. If the list is null then data with all
     * client codes owned by the user will be returned. If the list is empty no data will be returned (the intersection in
     * this case will be empty).
     */
    private List<String> client;
    /**
     * The end date (<b>inclusive</b>) of the result set in the form of {@code yyyymmdd} string. By default, if the
     * end date is not set ({@code null}) it will be set to the start date. <b>Please note that the max date range is limited by
     * 31 days!</b>
     */
    private LocalDate endDate;
    /**
     * A list of fields to exclude from the result set. If the list is {@code null} or empty all the fields
     * will be returned.
     */
    private List<String> exclude;
    /**
     * A nullable list of portfolios/subfunds to return. The result set will contain data for the
     * intersection of the  portfolios is the list with the portfolios that the current user is permitted to access. If the
     * list is null all portfolios the user has access to will be returned. If the list is empty no data will be returned
     * (the intersection in this case will be empty).
     */
    private List<String> portfolio;
    /**
     * The start date of the result set in the form of {@code yyyymmdd} string. If the start date is not set
     * ({@code null}) then yesterday will be taken as a start date (<b>not last business day!</b>)
     */
    private LocalDate startDate;

    public Filter() {
    }

    public Filter clients(String... clients) {
        this.client = asList(clients);
        return this;
    }

    public Filter endDate(LocalDate value) {
        this.endDate = value;
        return this;
    }

    public Filter exclude(String... fields) {
        this.exclude = asList(fields);
        return this;
    }

    public List<String> getClient() {
        return client;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public List<String> getExclude() {
        return exclude;
    }

    public List<String> getPortfolio() {
        return portfolio;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Filter portfolios(String... portfolios) {
        this.portfolio = asList(portfolios);
        return this;
    }

    public void setClient(List<String> client) {
        this.client = client;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void setExclude(List<String> exclude) {
        this.exclude = exclude;
    }

    public void setPortfolio(List<String> portfolio) {
        this.portfolio = portfolio;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Filter startDate(LocalDate value) {
        this.startDate = value;
        return this;
    }
}
