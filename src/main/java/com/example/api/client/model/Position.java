package com.example.api.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * The class represents the Position model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Position {
    @JsonProperty("accrInterestsPositionCurrency")
    private Double accrInterestsPositionCurrency;
    @JsonProperty("accrInterestsSubFundCurrency")
    private Double accrInterestsSubFundCurrency;
    @JsonProperty("baseGainLoss")
    private Double baseGainLoss;
    @JsonProperty("bookValue")
    private Double bookValue;
    @JsonProperty("bookValuePositionCurrency")
    private Double bookValuePositionCurrency;
    @JsonProperty("bookValueSubFundCurrency")
    private Double bookValueSubFundCurrency;
    @JsonProperty("brokerLongName")
    private String brokerLongName;
    @JsonProperty("class2Name")
    private String class2Name;
    @JsonProperty("class3Name")
    private String class3Name;
    @JsonProperty("commission")
    private Double commission;
    @JsonProperty("cusip")
    private String cusip;
    @JsonProperty("discountRate")
    private Double discountRate;
    @JsonProperty("exchangeRate")
    private double exchangeRate;
    @JsonProperty("expiryDate")
    private String expiryDate;
    @JsonProperty("fee")
    private Double fee;
    @JsonProperty("fwdLongAmount")
    private Double fwdLongAmount;
    @JsonProperty("fwdLongCurrency")
    private String fwdLongCurrency;
    @JsonProperty("fwdShortAmount")
    private Double fwdShortAmount;
    @JsonProperty("fwdShortCurrency")
    private String fwdShortCurrency;
    @JsonProperty("interestRate")
    private Double interestRate;
    @JsonProperty("internalPositionId")
    private String internalPositionId;
    @JsonProperty("issueCountry")
    private String issueCountry;
    @JsonProperty("localGainLoss")
    private Double localGainLoss;
    @JsonProperty("marketPrice")
    private BigDecimal marketPrice;
    @JsonProperty("marketValuePositionCurrency")
    private BigDecimal marketValuePositionCurrency;
    @JsonProperty("marketValueSubFundCurrency")
    private BigDecimal marketValueSubFundCurrency;
    @JsonProperty("maturityDate")
    private String maturityDate;
    @JsonProperty("navDate")
    private String navDate;
    @JsonProperty("positionCurrency")
    private String positionCurrency;
    @JsonProperty("positionName")
    private String positionName;
    @JsonProperty("positionQuantity")
    private BigDecimal positionQuantity;
    @JsonProperty("positionType")
    private String positionType;
    @JsonProperty("realized")
    private Double realized;
    @JsonProperty("securityBic")
    private String securityBic;
    @JsonProperty("securityRic")
    private String securityRic;
    @JsonProperty("securitySubType")
    private String securitySubType;
    @JsonProperty("sedol")
    private String sedol;
    @JsonProperty("settlementDate")
    private String settlementDate;
    @JsonProperty("sourceAccountName")
    private String sourceAccountName;
    @JsonProperty("status")
    private String status;
    @JsonProperty("strikeRate")
    private Double strikeRate;
    @JsonProperty("subFundIdPosition")
    private String subFundIdPosition;
    @JsonProperty("swapId")
    private String swapId;
    @JsonProperty("tradeDate")
    private String tradeDate;
    @JsonProperty("unrealizedGainLossPercent")
    private Double unrealizedGainLossPercent;
    @JsonProperty("unrealizedGainLossPositionCurrency")
    private Double unrealizedGainLossPositionCurrency;
    @JsonProperty("unrealizedGainLossSubFundCurrency")
    private Double unrealizedGainLossSubFundCurrency;
    @JsonProperty("weightTNav")
    private double weightTNav;

    public Double getAccrInterestsPositionCurrency() {
        return accrInterestsPositionCurrency;
    }

    public Double getAccrInterestsSubFundCurrency() {
        return accrInterestsSubFundCurrency;
    }

    public Double getBaseGainLoss() {
        return baseGainLoss;
    }

    public Double getBookValue() {
        return bookValue;
    }

    public Double getBookValuePositionCurrency() {
        return bookValuePositionCurrency;
    }

    public Double getBookValueSubFundCurrency() {
        return bookValueSubFundCurrency;
    }

    public String getBrokerLongName() {
        return brokerLongName;
    }

    public String getClass2Name() {
        return class2Name;
    }

    public String getClass3Name() {
        return class3Name;
    }

    public Double getCommission() {
        return commission;
    }

    public String getCusip() {
        return cusip;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public Double getFee() {
        return fee;
    }

    public Double getFwdLongAmount() {
        return fwdLongAmount;
    }

    public String getFwdLongCurrency() {
        return fwdLongCurrency;
    }

    public Double getFwdShortAmount() {
        return fwdShortAmount;
    }

    public String getFwdShortCurrency() {
        return fwdShortCurrency;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public String getInternalPositionId() {
        return internalPositionId;
    }

    public String getIssueCountry() {
        return issueCountry;
    }

    public Double getLocalGainLoss() {
        return localGainLoss;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public BigDecimal getMarketValuePositionCurrency() {
        return marketValuePositionCurrency;
    }

    public BigDecimal getMarketValueSubFundCurrency() {
        return marketValueSubFundCurrency;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public String getNavDate() {
        return navDate;
    }

    public String getPositionCurrency() {
        return positionCurrency;
    }

    public String getPositionName() {
        return positionName;
    }

    public BigDecimal getPositionQuantity() {
        return positionQuantity;
    }

    public String getPositionType() {
        return positionType;
    }

    public Double getRealized() {
        return realized;
    }

    public String getSecurityBic() {
        return securityBic;
    }

    public String getSecurityRic() {
        return securityRic;
    }

    public String getSecuritySubType() {
        return securitySubType;
    }

    public String getSedol() {
        return sedol;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public String getSourceAccountName() {
        return sourceAccountName;
    }

    public String getStatus() {
        return status;
    }

    public Double getStrikeRate() {
        return strikeRate;
    }

    public String getSubFundIdPosition() {
        return subFundIdPosition;
    }

    public String getSwapId() {
        return swapId;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public Double getUnrealizedGainLossPercent() {
        return unrealizedGainLossPercent;
    }

    public Double getUnrealizedGainLossPositionCurrency() {
        return unrealizedGainLossPositionCurrency;
    }

    public Double getUnrealizedGainLossSubFundCurrency() {
        return unrealizedGainLossSubFundCurrency;
    }

    public double getWeightTNav() {
        return weightTNav;
    }

    public void setAccrInterestsPositionCurrency(Double accrInterestsPositionCurrency) {
        this.accrInterestsPositionCurrency = accrInterestsPositionCurrency;
    }

    public void setAccrInterestsSubFundCurrency(Double accrInterestsSubFundCurrency) {
        this.accrInterestsSubFundCurrency = accrInterestsSubFundCurrency;
    }

    public void setBaseGainLoss(Double baseGainLoss) {
        this.baseGainLoss = baseGainLoss;
    }

    public void setBookValue(Double bookValue) {
        this.bookValue = bookValue;
    }

    public void setBookValuePositionCurrency(Double bookValuePositionCurrency) {
        this.bookValuePositionCurrency = bookValuePositionCurrency;
    }

    public void setBookValueSubFundCurrency(Double bookValueSubFundCurrency) {
        this.bookValueSubFundCurrency = bookValueSubFundCurrency;
    }

    public void setBrokerLongName(String brokerLongName) {
        this.brokerLongName = brokerLongName;
    }

    public void setClass2Name(String class2Name) {
        this.class2Name = class2Name;
    }

    public void setClass3Name(String class3Name) {
        this.class3Name = class3Name;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public void setCusip(String cusip) {
        this.cusip = cusip;
    }

    public void setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public void setFwdLongAmount(Double fwdLongAmount) {
        this.fwdLongAmount = fwdLongAmount;
    }

    public void setFwdLongCurrency(String fwdLongCurrency) {
        this.fwdLongCurrency = fwdLongCurrency;
    }

    public void setFwdShortAmount(Double fwdShortAmount) {
        this.fwdShortAmount = fwdShortAmount;
    }

    public void setFwdShortCurrency(String fwdShortCurrency) {
        this.fwdShortCurrency = fwdShortCurrency;
    }

    public void setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
    }

    public void setInternalPositionId(String internalPositionId) {
        this.internalPositionId = internalPositionId;
    }

    public void setIssueCountry(String issueCountry) {
        this.issueCountry = issueCountry;
    }

    public void setLocalGainLoss(Double localGainLoss) {
        this.localGainLoss = localGainLoss;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public void setMarketValuePositionCurrency(BigDecimal marketValuePositionCurrency) {
        this.marketValuePositionCurrency = marketValuePositionCurrency;
    }

    public void setMarketValueSubFundCurrency(BigDecimal marketValueSubFundCurrency) {
        this.marketValueSubFundCurrency = marketValueSubFundCurrency;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public void setNavDate(String navDate) {
        this.navDate = navDate;
    }

    public void setPositionCurrency(String positionCurrency) {
        this.positionCurrency = positionCurrency;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public void setPositionQuantity(BigDecimal positionQuantity) {
        this.positionQuantity = positionQuantity;
    }

    public void setPositionType(String positionType) {
        this.positionType = positionType;
    }

    public void setRealized(Double realized) {
        this.realized = realized;
    }

    public void setSecurityBic(String securityBic) {
        this.securityBic = securityBic;
    }

    public void setSecurityRic(String securityRic) {
        this.securityRic = securityRic;
    }

    public void setSecuritySubType(String securitySubType) {
        this.securitySubType = securitySubType;
    }

    public void setSedol(String sedol) {
        this.sedol = sedol;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public void setSourceAccountName(String sourceAccountName) {
        this.sourceAccountName = sourceAccountName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStrikeRate(Double strikeRate) {
        this.strikeRate = strikeRate;
    }

    public void setSubFundIdPosition(String subFundIdPosition) {
        this.subFundIdPosition = subFundIdPosition;
    }

    public void setSwapId(String swapId) {
        this.swapId = swapId;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public void setUnrealizedGainLossPercent(Double unrealizedGainLossPercent) {
        this.unrealizedGainLossPercent = unrealizedGainLossPercent;
    }

    public void setUnrealizedGainLossPositionCurrency(Double unrealizedGainLossPositionCurrency) {
        this.unrealizedGainLossPositionCurrency = unrealizedGainLossPositionCurrency;
    }

    public void setUnrealizedGainLossSubFundCurrency(Double unrealizedGainLossSubFundCurrency) {
        this.unrealizedGainLossSubFundCurrency = unrealizedGainLossSubFundCurrency;
    }

    public void setWeightTNav(double weightTNav) {
        this.weightTNav = weightTNav;
    }

    @Override
    public String toString() {
        return "Position[ positionName: " + positionName + ", marketPrice: " + marketPrice + ", " +
            "positionQuantity: " + positionQuantity + ", positionCurrency: " + positionCurrency + ", positionType: " + positionType + " ]";
    }
}