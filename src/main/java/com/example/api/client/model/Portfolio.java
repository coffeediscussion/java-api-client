package com.example.api.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * The class represents the Portfolio model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Portfolio {
    @JsonProperty("aggregateBalanceSheetPacks")
    private List<BalanceSheet> aggregateBalanceSheetPacks;
    @JsonProperty("aggregatePositionPacks")
    private List<Position> aggregatePositionPacks;
    @JsonProperty("aggregateProfitAndLossPacks")
    private List<ProfitAndLoss> aggregateProfitAndLossPacks;
    @JsonProperty("aggregateTransactionPacks")
    private List<Transaction> aggregateTransactionPacks;
    @JsonProperty("changePercentTNav")
    private BigDecimal changePercentTNav;
    @JsonProperty("clientId")
    private String clientId;
    @JsonProperty("_id")
    private String id;
    @JsonProperty("lastUpdated")
    private String lastUpdated;
    @JsonProperty("navDate")
    private String navDate;
    @JsonProperty("payBalSheetPercentToNetNav")
    private BigDecimal payBalSheetPercentToNetNav;
    @JsonProperty("payableBalanceSheet")
    private BigDecimal payableBalanceSheet;
    @JsonProperty("recBalSheetPercentToNetNav")
    private BigDecimal recBalSheetPercentToNetNav;
    @JsonProperty("receivableBalanceSheet")
    private BigDecimal receivableBalanceSheet;
    @JsonProperty("viewSharePack")
    private List<ShareClass> shareClasses;
    @JsonProperty("subFundCurrency")
    private String subFundCurrency;
    @JsonProperty("subFundId")
    private String subFundId;
    @JsonProperty("subFundName")
    private String subFundName;
    @JsonProperty("tNav")
    private BigDecimal tNav;

    public List<BalanceSheet> getAggregateBalanceSheetPacks() {
        return aggregateBalanceSheetPacks;
    }

    public List<Position> getAggregatePositionPacks() {
        return aggregatePositionPacks;
    }

    public List<ProfitAndLoss> getAggregateProfitAndLossPacks() {
        return aggregateProfitAndLossPacks;
    }

    public List<Transaction> getAggregateTransactionPacks() {
        return aggregateTransactionPacks;
    }

    public BigDecimal getChangePercentTNav() {
        return changePercentTNav;
    }

    public String getClientId() {
        return clientId;
    }

    public String getId() {
        return id;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public String getNavDate() {
        return navDate;
    }

    public BigDecimal getPayBalSheetPercentToNetNav() {
        return payBalSheetPercentToNetNav;
    }

    public BigDecimal getPayableBalanceSheet() {
        return payableBalanceSheet;
    }

    public BigDecimal getRecBalSheetPercentToNetNav() {
        return recBalSheetPercentToNetNav;
    }

    public BigDecimal getReceivableBalanceSheet() {
        return receivableBalanceSheet;
    }

    public List<ShareClass> getShareClasses() {
        return shareClasses;
    }

    public String getSubFundCurrency() {
        return subFundCurrency;
    }

    public String getSubFundId() {
        return subFundId;
    }

    public String getSubFundName() {
        return subFundName;
    }

    public BigDecimal getTNav() {
        return tNav;
    }

    public void setAggregateBalanceSheetPacks(
        List<BalanceSheet> aggregateBalanceSheetPacks) {
        this.aggregateBalanceSheetPacks = aggregateBalanceSheetPacks;
    }

    public void setAggregatePositionPacks(List<Position> aggregatePositionPacks) {
        this.aggregatePositionPacks = aggregatePositionPacks;
    }

    public void setAggregateProfitAndLossPacks(
        List<ProfitAndLoss> aggregateProfitAndLossPacks) {
        this.aggregateProfitAndLossPacks = aggregateProfitAndLossPacks;
    }

    public void setAggregateTransactionPacks(List<Transaction> aggregateTransactionPacks) {
        this.aggregateTransactionPacks = aggregateTransactionPacks;
    }

    public void setChangePercentTNav(BigDecimal changePercentTNav) {
        this.changePercentTNav = changePercentTNav;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setNavDate(String navDate) {
        this.navDate = navDate;
    }

    public void setPayBalSheetPercentToNetNav(BigDecimal payBalSheetPercentToNetNav) {
        this.payBalSheetPercentToNetNav = payBalSheetPercentToNetNav;
    }

    public void setPayableBalanceSheet(BigDecimal payableBalanceSheet) {
        this.payableBalanceSheet = payableBalanceSheet;
    }

    public void setRecBalSheetPercentToNetNav(BigDecimal recBalSheetPercentToNetNav) {
        this.recBalSheetPercentToNetNav = recBalSheetPercentToNetNav;
    }

    public void setReceivableBalanceSheet(BigDecimal receivableBalanceSheet) {
        this.receivableBalanceSheet = receivableBalanceSheet;
    }

    public void setShareClasses(List<ShareClass> shareClasses) {
        this.shareClasses = shareClasses;
    }

    public void setSubFundCurrency(String subFundCurrency) {
        this.subFundCurrency = subFundCurrency;
    }

    public void setSubFundId(String subFundId) {
        this.subFundId = subFundId;
    }

    public void setSubFundName(String subFundName) {
        this.subFundName = subFundName;
    }

    public void setTNav(BigDecimal tNav) {
        this.tNav = tNav;
    }

    @Override
    public String toString() {
        return "Portfolio[ id: " + id + ", clientId: " + clientId + ", subFundId: " + subFundId + ", subFundName: " + subFundName + ", " +
            "subFundCurrency: " + subFundCurrency + " ]";
    }
}