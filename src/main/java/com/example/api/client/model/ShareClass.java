package com.example.api.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class represents the View Share Pack model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShareClass {
    @JsonProperty("changePercent")
    private Double changePercent;
    @JsonProperty("clientByShare")
    private String clientByShare;
    @JsonProperty("differencePercent")
    private Double differencePercent;
    @JsonProperty("distributionFactor")
    private Double distributionFactor;
    @JsonProperty("distributionFactorGains")
    private Double distributionFactorGains;
    @JsonProperty("distributionFactorIncome")
    private Double distributionFactorIncome;
    @JsonProperty("nav")
    private Double nav;
    @JsonProperty("navDate")
    private String navDate;
    @JsonProperty("outstandingShares")
    private Double outstandingShares;
    @JsonProperty("shareClass")
    private String shareClass;
    @JsonProperty("shareClassCurrency")
    private String shareClassCurrency;
    @JsonProperty("shareClassId")
    private String shareClassId;
    @JsonProperty("tNav")
    private Double tNav;
    @JsonProperty("totalInflow")
    private Double totalInflow;
    @JsonProperty("totalOutflow")
    private Double totalOutflow;

    public Double getChangePercent() {
        return changePercent;
    }

    public String getClientByShare() {
        return clientByShare;
    }

    public Double getDifferencePercent() {
        return differencePercent;
    }

    public Double getDistributionFactor() {
        return distributionFactor;
    }

    public Double getDistributionFactorGains() {
        return distributionFactorGains;
    }

    public Double getDistributionFactorIncome() {
        return distributionFactorIncome;
    }

    public Double getNav() {
        return nav;
    }

    public String getNavDate() {
        return navDate;
    }

    public Double getOutstandingShares() {
        return outstandingShares;
    }

    public String getShareClass() {
        return shareClass;
    }

    public String getShareClassCurrency() {
        return shareClassCurrency;
    }

    public String getShareClassId() {
        return shareClassId;
    }

    public Double getTNav() {
        return tNav;
    }

    public Double getTotalInflow() {
        return totalInflow;
    }

    public Double getTotalOutflow() {
        return totalOutflow;
    }

    public void setChangePercent(Double changePercent) {
        this.changePercent = changePercent;
    }

    public void setClientByShare(String clientByShare) {
        this.clientByShare = clientByShare;
    }

    public void setDifferencePercent(Double differencePercent) {
        this.differencePercent = differencePercent;
    }

    public void setDistributionFactor(Double distributionFactor) {
        this.distributionFactor = distributionFactor;
    }

    public void setDistributionFactorGains(Double distributionFactorGains) {
        this.distributionFactorGains = distributionFactorGains;
    }

    public void setDistributionFactorIncome(Double distributionFactorIncome) {
        this.distributionFactorIncome = distributionFactorIncome;
    }

    public void setNav(Double nav) {
        this.nav = nav;
    }

    public void setNavDate(String navDate) {
        this.navDate = navDate;
    }

    public void setOutstandingShares(Double outstandingShares) {
        this.outstandingShares = outstandingShares;
    }

    public void setShareClass(String shareClass) {
        this.shareClass = shareClass;
    }

    public void setShareClassCurrency(String shareClassCurrency) {
        this.shareClassCurrency = shareClassCurrency;
    }

    public void setShareClassId(String shareClassId) {
        this.shareClassId = shareClassId;
    }

    public void setTNav(Double tNav) {
        this.tNav = tNav;
    }

    public void setTotalInflow(Double totalInflow) {
        this.totalInflow = totalInflow;
    }

    public void setTotalOutflow(Double totalOutflow) {
        this.totalOutflow = totalOutflow;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[ shareClassId: " + shareClassId + ", shareClass: " + shareClass +
            ", shareClassCurrency: " + shareClassCurrency + " ]";
    }
}