package com.example.api.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class represents the Transaction model. Please note this class is for demo purposes only and therefore
 * it could be incomplete. Please refer to the Swagger page for up-to-date information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {
    @JsonProperty("accruedInterestPositionCurrency")
    private Double accruedInterestPositionCurrency;
    @JsonProperty("accruedInterestSubFundCurrency")
    private Double accruedInterestSubFundCurrency;
    @JsonProperty("adjustmentType")
    private String adjustmentType;
    @JsonProperty("baseProceeds")
    private Double baseProceeds;
    @JsonProperty("bloombergId")
    private String bloombergId;
    @JsonProperty("bookValuePositionCurrency")
    private Double bookValuePositionCurrency;
    @JsonProperty("class2Name")
    private String class2Name;
    @JsonProperty("class3Name")
    private String class3Name;
    @JsonProperty("counterPartyId")
    private String counterPartyId;
    @JsonProperty("counterPartyName")
    private String counterPartyName;
    @JsonProperty("counterPartyShortName")
    private String counterPartyShortName;
    @JsonProperty("cusip")
    private String cusip;
    @JsonProperty("divIncomeBase")
    private Double divIncomeBase;
    @JsonProperty("divIncomeLocal")
    private Double divIncomeLocal;
    @JsonProperty("exRate")
    private Double exRate;
    @JsonProperty("expiryDate")
    private String expiryDate;
    @JsonProperty("fundName")
    private String fundName;
    @JsonProperty("internalPositionId")
    private String internalPositionId;
    @JsonProperty("localProceeds")
    private Double localProceeds;
    @JsonProperty("marketPrice")
    private Double marketPrice;
    @JsonProperty("netDividendLocal")
    private Double netDividendLocal;
    @JsonProperty("openCloseIndicator")
    private String openCloseIndicator;
    @JsonProperty("positionCurrency")
    private String positionCurrency;
    @JsonProperty("positionName")
    private String positionName;
    @JsonProperty("positionType")
    private String positionType;
    @JsonProperty("quantity")
    private Double quantity;
    @JsonProperty("realizedResultSubFundCurrency")
    private Double realizedResultSubFundCurrency;
    @JsonProperty("reutersId")
    private String reutersId;
    @JsonProperty("sedol")
    private String sedol;
    @JsonProperty("settleAmountPositionCurrency")
    private Double settleAmountPositionCurrency;
    @JsonProperty("settleAmountSubFundCurrency")
    private Double settleAmountSubFundCurrency;
    @JsonProperty("settleCurrency")
    private String settleCurrency;
    @JsonProperty("sourceAccountNumber")
    private String sourceAccountNumber;
    @JsonProperty("subFundIdTransaction")
    private String subFundIdTransaction;
    @JsonProperty("tradeBrokerFees")
    private Double tradeBrokerFees;
    @JsonProperty("tradeCommissions")
    private Double tradeCommissions;
    @JsonProperty("tradeDate")
    private String tradeDate;
    @JsonProperty("transactionId")
    private String transactionId;
    @JsonProperty("transactionType")
    private String transactionType;
    @JsonProperty("valueDate")
    private String valueDate;
    @JsonProperty("weightRealizedResultSubFundTNav")
    private Double weightRealizedResultSubFundTNav;

    public Double getAccruedInterestPositionCurrency() {
        return accruedInterestPositionCurrency;
    }

    public Double getAccruedInterestSubFundCurrency() {
        return accruedInterestSubFundCurrency;
    }

    public String getAdjustmentType() {
        return adjustmentType;
    }

    public Double getBaseProceeds() {
        return baseProceeds;
    }

    public String getBloombergId() {
        return bloombergId;
    }

    public Double getBookValuePositionCurrency() {
        return bookValuePositionCurrency;
    }

    public String getClass2Name() {
        return class2Name;
    }

    public String getClass3Name() {
        return class3Name;
    }

    public String getCounterPartyId() {
        return counterPartyId;
    }

    public String getCounterPartyName() {
        return counterPartyName;
    }

    public String getCounterPartyShortName() {
        return counterPartyShortName;
    }

    public String getCusip() {
        return cusip;
    }

    public Double getDivIncomeBase() {
        return divIncomeBase;
    }

    public Double getDivIncomeLocal() {
        return divIncomeLocal;
    }

    public Double getExRate() {
        return exRate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getFundName() {
        return fundName;
    }

    public String getInternalPositionId() {
        return internalPositionId;
    }

    public Double getLocalProceeds() {
        return localProceeds;
    }

    public Double getMarketPrice() {
        return marketPrice;
    }

    public Double getNetDividendLocal() {
        return netDividendLocal;
    }

    public String getOpenCloseIndicator() {
        return openCloseIndicator;
    }

    public String getPositionCurrency() {
        return positionCurrency;
    }

    public String getPositionName() {
        return positionName;
    }

    public String getPositionType() {
        return positionType;
    }

    public Double getQuantity() {
        return quantity;
    }

    public Double getRealizedResultSubFundCurrency() {
        return realizedResultSubFundCurrency;
    }

    public String getReutersId() {
        return reutersId;
    }

    public String getSedol() {
        return sedol;
    }

    public Double getSettleAmountPositionCurrency() {
        return settleAmountPositionCurrency;
    }

    public Double getSettleAmountSubFundCurrency() {
        return settleAmountSubFundCurrency;
    }

    public String getSettleCurrency() {
        return settleCurrency;
    }

    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public String getSubFundIdTransaction() {
        return subFundIdTransaction;
    }

    public Double getTradeBrokerFees() {
        return tradeBrokerFees;
    }

    public Double getTradeCommissions() {
        return tradeCommissions;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getValueDate() {
        return valueDate;
    }

    public Double getWeightRealizedResultSubFundTNav() {
        return weightRealizedResultSubFundTNav;
    }

    public void setAccruedInterestPositionCurrency(Double accruedInterestPositionCurrency) {
        this.accruedInterestPositionCurrency = accruedInterestPositionCurrency;
    }

    public void setAccruedInterestSubFundCurrency(Double accruedInterestSubFundCurrency) {
        this.accruedInterestSubFundCurrency = accruedInterestSubFundCurrency;
    }

    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    public void setBaseProceeds(Double baseProceeds) {
        this.baseProceeds = baseProceeds;
    }

    public void setBloombergId(String bloombergId) {
        this.bloombergId = bloombergId;
    }

    public void setBookValuePositionCurrency(Double bookValuePositionCurrency) {
        this.bookValuePositionCurrency = bookValuePositionCurrency;
    }

    public void setClass2Name(String class2Name) {
        this.class2Name = class2Name;
    }

    public void setClass3Name(String class3Name) {
        this.class3Name = class3Name;
    }

    public void setCounterPartyId(String counterPartyId) {
        this.counterPartyId = counterPartyId;
    }

    public void setCounterPartyName(String counterPartyName) {
        this.counterPartyName = counterPartyName;
    }

    public void setCounterPartyShortName(String counterPartyShortName) {
        this.counterPartyShortName = counterPartyShortName;
    }

    public void setCusip(String cusip) {
        this.cusip = cusip;
    }

    public void setDivIncomeBase(Double divIncomeBase) {
        this.divIncomeBase = divIncomeBase;
    }

    public void setDivIncomeLocal(Double divIncomeLocal) {
        this.divIncomeLocal = divIncomeLocal;
    }

    public void setExRate(Double exRate) {
        this.exRate = exRate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public void setInternalPositionId(String internalPositionId) {
        this.internalPositionId = internalPositionId;
    }

    public void setLocalProceeds(Double localProceeds) {
        this.localProceeds = localProceeds;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public void setNetDividendLocal(Double netDividendLocal) {
        this.netDividendLocal = netDividendLocal;
    }

    public void setOpenCloseIndicator(String openCloseIndicator) {
        this.openCloseIndicator = openCloseIndicator;
    }

    public void setPositionCurrency(String positionCurrency) {
        this.positionCurrency = positionCurrency;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public void setPositionType(String positionType) {
        this.positionType = positionType;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public void setRealizedResultSubFundCurrency(Double realizedResultSubFundCurrency) {
        this.realizedResultSubFundCurrency = realizedResultSubFundCurrency;
    }

    public void setReutersId(String reutersId) {
        this.reutersId = reutersId;
    }

    public void setSedol(String sedol) {
        this.sedol = sedol;
    }

    public void setSettleAmountPositionCurrency(Double settleAmountPositionCurrency) {
        this.settleAmountPositionCurrency = settleAmountPositionCurrency;
    }

    public void setSettleAmountSubFundCurrency(Double settleAmountSubFundCurrency) {
        this.settleAmountSubFundCurrency = settleAmountSubFundCurrency;
    }

    public void setSettleCurrency(String settleCurrency) {
        this.settleCurrency = settleCurrency;
    }

    public void setSourceAccountNumber(String sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
    }

    public void setSubFundIdTransaction(String subFundIdTransaction) {
        this.subFundIdTransaction = subFundIdTransaction;
    }

    public void setTradeBrokerFees(Double tradeBrokerFees) {
        this.tradeBrokerFees = tradeBrokerFees;
    }

    public void setTradeCommissions(Double tradeCommissions) {
        this.tradeCommissions = tradeCommissions;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    public void setWeightRealizedResultSubFundTNav(Double weightRealizedResultSubFundTNav) {
        this.weightRealizedResultSubFundTNav = weightRealizedResultSubFundTNav;
    }

    @Override
    public String toString() {
        return "Transaction[ transactionId: " + transactionId + ", marketPrice: " + marketPrice + ", " +
            "quantity: " + quantity + ", positionCurrency: " + positionCurrency + ", transactionType: " + transactionType + " ]";
    }
}