package com.example.api.client.services;

import com.example.api.client.configuration.ApiEndpointConfiguration;
import com.example.api.client.model.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.function.Consumer;

import static java.util.Collections.*;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.MediaType.*;

/**
 * <p>The class represents one of the approaches in implementation of the interaction with the Data REST API.
 * One of the key points here is the authentication (and authorization) process. Before any requests to the Data
 * REST API the client application (service) must receive JWT token from the authentication/authorization service.
 * This token must be provided with each and every request to the rest of the services (i.e. Fund Accounting service).
 * The authentication process requires Client ID (aka App ID, Application ID) and Client Secret. The credentials
 * are provided by RBC.</p>
 * <p>The Data REST API are available for the clients in two environments: QA and PROD. The QA environment should be
 * used for test and demo purposes. The PROD environment URL is provided by RBC for each client separately.</p>
 */
class BaseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseService.class);
    private final ApiEndpointConfiguration apiEndpointConfiguration;
    private final Authorization authorization;
    private final RestTemplate restTemplate;

    protected BaseService(
        ApiEndpointConfiguration apiEndpointConfiguration,
        Authorization authorization,
        RestTemplateBuilder restTemplateBuilder
    ) {
        this.apiEndpointConfiguration = apiEndpointConfiguration;
        this.authorization = authorization;
        this.restTemplate = restTemplateBuilder.build();
    }

    private <T, R> R checkResponse(RequestEntity<T> entity, ResponseEntity<R> response) {
        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }
        LOGGER.error("Url: {}, Method: {}, Status: {}", entity.getUrl(), entity.getMethod(), response.getStatusCode());
        throw new HttpClientErrorException(response.getStatusCode());
    }

    private RequestEntity<Object> createRequestEntity(Consumer<UriComponentsBuilder> apply) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(singletonList(APPLICATION_JSON));
        headers.set("Authorization", getToken());
        return new RequestEntity<>(headers, GET, apiEndpointConfiguration.url(apply));
    }

    /**
     * <p>A utility method to send a GET request to the Data REST API. It builds an endpoint based on the ROOT_PATH,
     * service path and query string. It also adds default headers to the request, including the acceptable
     * media type and authorization header value (which is the JWT token received earlier).</p>
     *
     * @implNote Please note that the current demo accepts JSON only. However the Data REST API services may provide data
     * in other formats as well. Please refer to the Swagger web page for more details.
     */
    <R> R get(Class<R> responseType, Consumer<UriComponentsBuilder> apply) {
        return sendMessage(createRequestEntity(apply), responseType);
    }

    /**
     * JWT token received from the authorization service
     */
    private String getToken() {
        final Token token = authorization.getToken();
        return token.getTokenType() + " " + token.getAccessToken();
    }

    <R> R list(ParameterizedTypeReference<R> responseType, Consumer<UriComponentsBuilder> apply) {
        return sendMessage(createRequestEntity(apply), responseType);
    }

    /**
     * <p>A utility method that sends an HTTP request, checks the status of the response
     * and returns content of the response, if the response is successful; otherwise it throws an HTTP-specific
     * exception.</p>
     */
    private <T, R> R sendMessage(RequestEntity<T> entity, Class<R> responseType) {
        return checkResponse(entity, restTemplate.exchange(entity, responseType));
    }

    private <T, R> R sendMessage(RequestEntity<T> entity, ParameterizedTypeReference<R> responseType) {
        return checkResponse(entity, restTemplate.exchange(entity, responseType));
    }
}