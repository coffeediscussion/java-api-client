package com.example.api.client.services;

import com.example.api.client.configuration.ApiEndpointConfiguration;
import com.example.api.client.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.time.format.DateTimeFormatter.*;

/**
 * <p>The class is a sample of domain-specific service implementation. This class represents Fund Accounting service.
 * Please note that the class is for demo-purposes only and may be incomplete in terms of business functionality.
 * The purpose of the class is to demo how the client's application/service can interact with the Data REST API.
 * For full list of supported endpoints please refer to the Swagger web page.</p>
 * <p>All Fund Accounting service methods that return a list of items support pagination and date range.
 * Even though the page and size parameters are optional on the Data REST API side we highly encourage you to
 * make them mandatory on the client side. Considering the high volume of data, not specifying the parameters
 * may cause interruptions and service quality degradation during the data transfer between the servers and
 * clients.</p>
 *
 * @see com.example.api.client.services.BaseService
 */
@Service
public class FundAccountingService
    extends BaseService {

    @Autowired
    public FundAccountingService(
        ApiEndpointConfiguration apiEndpointConfiguration,
        Authorization authorization,
        RestTemplateBuilder restTemplateBuilder
    ) {
        super(apiEndpointConfiguration, authorization, restTemplateBuilder);
    }

    /**
     * Returns all details for the given id. If the record is not found the service will throw 404 error. This method
     * should be used if all details are required. If only specific subcollections are required then the specialized
     * methods are more preferable.
     *
     * @param id an id of a specific record. To discover IDs for a given set of records please refer to {@code list} method.
     */
    public Portfolio findById(String id) {
        return get(Portfolio.class, builder -> builder.pathSegment("fa", "v2", "portfolios", id));
    }

    public List<Portfolio> list() {
        return list(new Filter(), 0, 25);
    }

    /**
     * Returns a summary list of portfolios limited by the filter and pagination.
     *
     * @param filter query parameters to limit the result set. See {@code Filter} for more details.
     * @param page   a page of the result set to return.
     * @param size   a size of the page.
     */
    public List<Portfolio> list(Filter filter, int page, int size) {
        final ParameterizedTypeReference<List<Portfolio>> typeReference = new ParameterizedTypeReference<List<Portfolio>>() {
        };
        return list(typeReference, builder -> {
            builder.pathSegment("fa", "v2", "portfolios")
                .queryParam("page", page)
                .queryParam("size", size);
            if (filter.getClient() != null) {
                filter.getClient().forEach(it -> builder.queryParam("client", it));
            }
            if (filter.getPortfolio() != null) {
                filter.getPortfolio().forEach(it -> builder.queryParam("portfolio", it));
            }
            if (filter.getExclude() != null) {
                filter.getExclude().forEach(it -> builder.queryParam("exclude", it));
            }
            if (filter.getStartDate() != null) {
                builder.queryParam("startDate", filter.getStartDate().format(BASIC_ISO_DATE));
            }
            if (filter.getEndDate() != null) {
                builder.queryParam("endDate", filter.getEndDate().format(BASIC_ISO_DATE));
            }
        });
    }

    public List<BalanceSheet> listBalanceSheets(String id, List<String> exclude) {
        final ParameterizedTypeReference<List<BalanceSheet>> typeReference = new ParameterizedTypeReference<List<BalanceSheet>>() {
        };
        return listEntityById(id, "balancesheets", exclude, typeReference);
    }

    /**
     * A generic method that retrieves portfolio-specific details such as balance sheets, positions, transactions,
     * etc. It requests items from the Data REST API, converts them from JSON to a domain class and returns a list of the
     * converted items.
     *
     * <p><i>Please refer to the Swagger web page for full list of supported endpoints.</i></p>
     *
     * @param id            an id of a specific record
     * @param entity        a details-specific portion of the endpoint (such as "transactions")
     * @param typeReference the type of the domain class
     * @return a list of entities
     */
    private <T> List<T> listEntityById(String id, String entity, List<String> exclude,
        ParameterizedTypeReference<List<T>> typeReference) {
        return list(typeReference, builder -> {
            builder.pathSegment("fa", "v2", "portfolios", id, entity);
            exclude.forEach(it -> builder.queryParam("exclude", it));
            }
        );
    }

    public List<Position> listPositions(String id, List<String> exclude) {
        final ParameterizedTypeReference<List<Position>> typeReference = new ParameterizedTypeReference<List<Position>>() {
        };
        return listEntityById(id, "positions", exclude, typeReference);
    }

    public List<ProfitAndLoss> listProfitAndLosses(String id, List<String> exclude) {
        final ParameterizedTypeReference<List<ProfitAndLoss>> typeReference = new ParameterizedTypeReference<List<ProfitAndLoss>>() {
        };
        return listEntityById(id, "profitandlosses", exclude, typeReference);
    }

    public List<ShareClass> listShareClasses(String id, List<String> exclude) {
        final ParameterizedTypeReference<List<ShareClass>> typeReference = new ParameterizedTypeReference<List<ShareClass>>() {
        };
        return listEntityById(id, "shareclasses", exclude, typeReference);
    }

    public List<Transaction> listTransactions(String id, List<String> exclude) {
        final ParameterizedTypeReference<List<Transaction>> typeReference = new ParameterizedTypeReference<List<Transaction>>() {
        };
        return listEntityById(id, "transactions", exclude, typeReference);
    }
}