package com.example.api.client.services;

import com.example.api.client.configuration.AuthEndpointConfiguration;
import com.example.api.client.model.Token;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.UncheckedIOException;

import static java.util.Collections.*;
import static java.util.Optional.*;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.http.MediaType.*;

/**
 * @author Eugene Ossipov
 * A sample of Authorization service implementation. The token is retrieved on a first call. *Please note:* The service
 * does not implement the renewal of the JWT.
 */
@Service
public class Authorization {

    static final MediaType TEXT_PLAIN_UTF8 = MediaType.valueOf("text/plain;charset=UTF-8");
    private final AuthEndpointConfiguration authEndpointConfiguration;
    private final ObjectMapper objectMapper;
    private final RestTemplate restTemplate;
    private Token token = null;

    public Authorization(
        AuthEndpointConfiguration authEndpointConfiguration,
        ObjectMapper objectMapper,
        RestTemplateBuilder restTemplateBuilder
    ) {
        this.authEndpointConfiguration = authEndpointConfiguration;
        this.objectMapper = objectMapper;
        restTemplate = restTemplateBuilder.build();
    }

    /**
     * The method implements the process of authorization of the client app/service. Please note that the Client ID
     * and Client Secret must be passed as application/x-www-form-urlencoded content.
     */
    Token authorize() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(singletonList(APPLICATION_JSON));
        headers.setContentType(APPLICATION_FORM_URLENCODED);

        final MultiValueMap<String, String> values = new LinkedMultiValueMap<>();
        values.add("client_id", authEndpointConfiguration.getClientId());
        values.add("client_secret", authEndpointConfiguration.getClientSecret());
        values.add("grant_type", "client_credentials");
        values.add("scope", "read");

        final RequestEntity<MultiValueMap<String, String>> entity =
            new RequestEntity<>(values, headers, POST, authEndpointConfiguration.url());
        final ResponseEntity<String> response = restTemplate.exchange(entity, String.class);

        final MediaType contentType = response.getHeaders().getContentType();
        if (APPLICATION_JSON.equals(contentType) || APPLICATION_JSON_UTF8.equals(contentType)) {
            try {
                return objectMapper.readValue(response.getBody(), Token.class);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        } else if (TEXT_PLAIN.equals(contentType) || TEXT_PLAIN_UTF8.equals(contentType)) {
            return ofNullable(response.getBody())
                .map(it -> it.split("\\s+"))
                .filter(it -> it.length == 2)
                .map(it -> new Token(it[1]))
                .orElseThrow(() -> new RuntimeException("Invalid token format: " + response.getBody()));

        } else {
            throw new RuntimeException("Unsupported media type: " + contentType);
        }
    }

    synchronized Token getToken() {
        if (token == null) token = authorize();
        return token;
    }
}
