package com.example.api.client;

import com.example.api.client.model.Filter;
import com.example.api.client.model.Portfolio;
import com.example.api.client.services.FundAccountingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

import static java.util.Arrays.*;
import static java.util.Collections.*;

/**
 * The purpose of the program is to demo interaction with the Data REST API. The code below is for demo purposes only.
 */
@SpringBootApplication
public class Application
    implements CommandLineRunner {

    private final FundAccountingService service;

    @Autowired
    public Application(FundAccountingService service) {
        this.service = service;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        // List all records for the given date
        final Filter filter = new Filter();
        final List<Portfolio> portfolios = service.list(filter, 0, 10);
        portfolios.forEach(System.out::println);

        portfolios.stream().findFirst().ifPresent(it -> {
            System.out.println(service.findById(it.getId()));

            // List of balance sheets for the given record
            service.listBalanceSheets(it.getId(), emptyList()).forEach(System.out::println);

            // List of PNLs for the given portfolio
            service.listProfitAndLosses(it.getId(), emptyList()).forEach(System.out::println);

            // List of positions for the given portfolio
            service.listPositions(it.getId(), asList("positionName", "sedol")).forEach(System.out::println);

            service.listShareClasses(it.getId(), emptyList()).forEach(System.out::println);

            // List of transactions for the given portfolio
            service.listTransactions(it.getId(), emptyList()).forEach(System.out::println);
        });
    }
}
