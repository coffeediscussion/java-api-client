package com.example.api.client.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author Eugene Ossipov
 */
@Configuration
@ConfigurationProperties(prefix = "rbcone.api")
@Component
public class ApiEndpointConfiguration
    extends EndpointConfiguration {
}
