package com.example.api.client.configuration;

import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.function.Consumer;

/**
 * @author Eugene Ossipov
 */
public class EndpointConfiguration {

    /**
     * Service host provided by the RBC One Data API team. In the test app we use QA environment.
     * Please contact the RBC One Data API team for production environment information.
     */
    private String host;
    /**
     * Root path for all endpoints
     */
    private String rootPath;
    /**
     * URL scheme.
     */
    private String scheme;

    public EndpointConfiguration() {
        this("localhost", "");
    }

    public EndpointConfiguration(String host, String rootPath) {
        this(host, rootPath, "https");
    }

    public EndpointConfiguration(String host, String rootPath, String scheme) {
        this.host = host;
        this.rootPath = rootPath;
        this.scheme = scheme;
    }

    public String getHost() {
        return host;
    }

    public String getRootPath() {
        return rootPath;
    }

    public String getScheme() {
        return scheme;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    @Override
    public String toString() {
        return String.format("%s{ %s }", getClass().getSimpleName(), values());
    }

    public URI url(Consumer<UriComponentsBuilder> apply) {
        final UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
            .scheme(scheme).host(host).path(rootPath);
        apply.accept(builder);
        return builder.build(true).toUri();
    }

    public URI url() {
        return UriComponentsBuilder.newInstance()
            .scheme(scheme).host(host).path(rootPath).build(true).toUri();
    }

    protected String values() {
        return "scheme=" + scheme + ",host=" + host + ",rootPath=" + rootPath;
    }
}
