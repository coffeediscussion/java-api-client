package com.example.api.client.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author Eugene Ossipov
 */
@Configuration
@ConfigurationProperties(prefix = "rbcone.auth")
@Component
public class AuthEndpointConfiguration
    extends EndpointConfiguration {

    private String clientId;
    private String clientSecret;

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Override
    protected String values() {
        return super.values() + ",clientId=" + clientId;
    }
}
