package com.example.api.client.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Configuration;

/**
 * @author Eugene Ossipov
 */
@Configuration
public class ServiceConfiguration {

    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
