package com.example.api.client.configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @author Eugene Ossipov
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApiEndpointConfiguration.class})
@EnableConfigurationProperties
public class ApiEndpointConfigurationTest {

    @Autowired
    private ApiEndpointConfiguration apiEndpointConfiguration;

    @Test
    public void testConfiguration() {
        assertEquals("https", apiEndpointConfiguration.getScheme());
        assertEquals("qa-api-rbcone.sterbc.com", apiEndpointConfiguration.getHost());
        assertEquals("/apiproxy", apiEndpointConfiguration.getRootPath());
    }
}