package com.example.api.client.configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @author Eugene Ossipov
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthEndpointConfiguration.class})
@EnableConfigurationProperties
public class AuthEndpointConfigurationTest {

    @Autowired
    private AuthEndpointConfiguration authEndpointConfiguration;

    @Test
    public void testConfiguration() {
        assertEquals("https", authEndpointConfiguration.getScheme());
        assertEquals("ssoa.sterbc.com", authEndpointConfiguration.getHost());
        assertEquals("/as/token.oauth2", authEndpointConfiguration.getRootPath());
        assertEquals("API-DEMO-CLIENT", authEndpointConfiguration.getClientId());
        assertEquals("a84330347f837ca83f8515bc920d7a6c8f79f81f", authEndpointConfiguration.getClientSecret());
    }
}