package com.example.api.client.configuration;

import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.*;

/**
 * @author Eugene Ossipov
 */
public class EndpointConfigurationTest {

    @Test
    public void url() {
        final URI url = new EndpointConfiguration("qa-api-rbcone.sterbc.com", "/apiproxy/auth").url();
        assertEquals("https://qa-api-rbcone.sterbc.com/apiproxy/auth", url.toString());
    }

    @Test
    public void url_path() {
        final URI url = new EndpointConfiguration("qa-api-rbcone.sterbc.com", "/apiproxy")
            .url(it -> it.pathSegment("auth"));
        assertEquals("https://qa-api-rbcone.sterbc.com/apiproxy/auth", url.toString());
    }

    @Test
    public void url_query() {
        final URI url = new EndpointConfiguration("qa-api-rbcone.sterbc.com", "/apiproxy")
            .url(it -> it.pathSegment("auth")
                .queryParam("page", 1)
                .queryParam("size", 25));
        assertEquals("https://qa-api-rbcone.sterbc.com/apiproxy/auth?page=1&size=25", url.toString());
    }
}