package com.example.api.client.services;

import com.example.api.client.configuration.AuthEndpointConfiguration;
import com.example.api.client.model.Token;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpClientErrorException;

import static com.example.api.client.services.Authorization.*;
import static org.junit.Assert.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

/**
 * @author Eugene Ossipov
 */
@Configuration
@EnableConfigurationProperties
@ComponentScan(includeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {Authorization.class})
}, basePackages = {"com.example.api.client.services"}, useDefaultFilters = false)
class AuthorizationTestConfiguration {
}

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthorizationTestConfiguration.class, AuthEndpointConfiguration.class})
@RestClientTest(Authorization.class)
public class AuthorizationTest {

    @Autowired
    private Authorization authorization;

    @Autowired
    private MockRestServiceServer server;

    @Test(expected = HttpClientErrorException.class)
    public void getToken_http_error() {
        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
            .andRespond(withBadRequest());

        authorization.authorize();
        fail();
    }

    @Test(expected = RuntimeException.class)
    public void getToken_invalid_media() {
        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
            .andRespond(withSuccess("Bear eyJhbGciOiJSU_XYZ", APPLICATION_FORM_URLENCODED));

        authorization.authorize();
        fail();
    }

    @Test
    public void getToken_json() {
        final String responseBody = "{\"access_token\":\"eyJhbGciOiJSU\",\"token_type\":\"Bear\",\"expires_in\":7200}";
        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
            .andRespond(withSuccess(responseBody, APPLICATION_JSON_UTF8));
        final Token token = authorization.authorize();

        assertEquals("eyJhbGciOiJSU", token.getAccessToken());
        assertEquals("Bear", token.getTokenType());
        assertEquals(7200, token.getExpiresIn());
    }

    @Test
    public void getToken_plain() {
        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
            .andRespond(withSuccess("Bear eyJhbGciOiJSU_XYZ", TEXT_PLAIN_UTF8));

        final Token token = authorization.authorize();
        assertEquals("eyJhbGciOiJSU_XYZ", token.getAccessToken());
        assertEquals("Bearer", token.getTokenType());
        assertEquals(7199, token.getExpiresIn());
    }

    @Test(expected = RuntimeException.class)
    public void getToken_plain_invalid() {
        server.expect(requestTo("https://ssoa.sterbc.com/as/token.oauth2"))
            .andRespond(withSuccess("eyJhbGciOiJSU_XYZ", TEXT_PLAIN_UTF8));

        authorization.authorize();
        fail();
    }
}