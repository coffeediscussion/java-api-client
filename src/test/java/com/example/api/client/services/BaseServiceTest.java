package com.example.api.client.services;

import com.example.api.client.configuration.ApiEndpointConfiguration;
import com.example.api.client.configuration.AuthEndpointConfiguration;
import com.example.api.client.model.Portfolio;
import com.example.api.client.model.Token;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

@Configuration
@ComponentScan(basePackages = {"com.example.api.client.services"})
class BaseServiceTestConfiguration {
}

/**
 * @author Eugene Ossipov
 */
@ActiveProfiles("test")
@EnableConfigurationProperties
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BaseServiceTestConfiguration.class, ApiEndpointConfiguration.class, AuthEndpointConfiguration.class})
@RestClientTest(FundAccountingService.class)
public class BaseServiceTest {

    @MockBean
    private Authorization authorization;
    @Autowired
    private MockRestServiceServer server;
    @Autowired
    private FundAccountingService service;

    @Test
    public void get() throws Exception {
        final Portfolio portfolio = new Portfolio();
        portfolio.setClientId("CI");

        final String responseBody = new ObjectMapper().writeValueAsString(Collections.singletonList(portfolio));
        server.expect(requestTo("https://qa-api-rbcone.sterbc.com/apiproxy/fa/v2/portfolios?page=0&size=25"))
            .andRespond(withSuccess(responseBody, APPLICATION_JSON_UTF8));

        final List<Portfolio> portfolios = service.list();
        assertNotNull(portfolios);
        assertEquals(1, portfolios.size());
    }

    @Before
    public void setUp() {
        given(authorization.getToken()).willReturn(new Token("XYZ"));
    }
}